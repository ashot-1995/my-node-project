const express=require('express');
const GetController=require('./../controller/GetController')
const router=express.Router();
const {body}=require("express-validator");
const {User}=require('./../model/user');
const PostController = require('../controller/PostController');
const passport=require('passport')
const LocalStrategy=require('passport-local').Strategy
const bcrypt=require('bcrypt')
const multer=require('multer')
const storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'public/img/')
    },
    filename:function (req,file,cb){
        let x =Date.now()+'-'+file.originalname;
        req.session.img = x
        cb(null,x)
    }
})
const upload=multer({storage:storage})

const isLoginedUser= (req, res, next)=>{
    console.log("user  ......" , req.user)
    console.log(req.isAuthenticated()&&req.user.type==0)
    console.log(req.isAuthenticated())
    if(req.isAuthenticated()&&req.user.type==0){
        return next();
    }
    res.redirect("/")
}
const isLoginedAdmin= (req, res, next)=>{
    console.log("user  ......" , req.user)
    if(req.isAuthenticated()&&req.user.type==1){
        return next();
    }
    res.redirect("/")
}
router.get('/',GetController.getIndex)

router.get('/register',GetController.getRegister)

router.get('/profile', isLoginedUser ,  GetController.getProfile)

router.get('/product',GetController.getProduct)

router.get('/friends',GetController.getFriends)

router.get('/chat',GetController.getChat)

router.get('/request',GetController.getRequest)

router.get('/search', GetController.search)

router.get('/adminProfile', isLoginedAdmin ,  GetController.adminProfile)


router.post('/usReg',[
    body('name').notEmpty().withMessage('enter your name').isAlpha().withMessage('enter string'),
    body('surname').notEmpty().withMessage('enter your surname').isAlpha().withMessage('enter string'),
    body('age').notEmpty().withMessage('enter your age').isNumeric().withMessage('enter number'),
    body('email').notEmpty().withMessage('enter your email').isEmail().withMessage('enter email'),
    body('password').notEmpty().withMessage('enter your password').isLength({min:5,max:10}).withMessage('password length error')
],PostController.register)

router.post('/picUrl',upload.single('picture'),async (req,res)=>{
    console.log("pic url", req.session.img)
    await User.update({image:req.session.img}, {where:{id:req.session.us_id}})
    res.redirect('/profile')
})

router.post('/login',
  passport.authenticate('local', {
                                   failureFlash: true }),
                                   (req,res)=>{
                                        if(req.user){
                                            req.session.us_id=req.user.id
                                            if(req.user.type==1){
                                                res.redirect('/adminProfile')
                                            }else{
                                                res.redirect('/profile')
                                            }
                                        }else{
                                            res.redirect('/')
                                        }
                                   }
);

router.post('/addProd',upload.single('image'),PostController.addProd)

router.post('/addFriend',PostController.addFriend)

router.post('/logOut',PostController.logOut)

router.post('/acceptFr',PostController.acceptFr)

router.post('/cancelFr',PostController.cancelFr)

router.post('/deleteFr',PostController.deleteFr)

passport.use('local', new LocalStrategy(
    async function(username, password, done) {
       console.log("before")
      let user=await User.findOne({where:{email:username}}) 
        if (!user) {
         done(null, false, { message: 'Incorrect username.' });
        }
        if(!bcrypt.compareSync(password, user.password)){
            done(null, false, { message: 'Incorrect password.' });

        }
        done(null, user);
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});
  
passport.deserializeUser(async function(id, done) {
    console.log(id)
    done(null, await User.findOne({where:{id}}))
});

module.exports= router