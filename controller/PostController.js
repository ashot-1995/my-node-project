const bcrypt=require('bcrypt')
const len=10;
const {body,validationResult}=require("express-validator");
const {User,sequelize,Product, Friend}=require('./../model/user');

class PostController{
    constructor(){
    }
    async register(req,res){
        let error=validationResult(req)
        let err=error.errors
        console.log(err)
        if(error.isEmpty()){
            let hash=bcrypt.hashSync(req.body.password,len)
            let x=await User.create({...req.body,password:hash});
            console.log(x);
            req.session.errors=undefined;
            req.flash('success_msg','you are registered');
        }
        else{
            req.session.errors=err
            req.flash('error_msg','you are not registered');
        }
        res.redirect('/register')
    }
    async addProd(req,res){
        let x=await Product.create({...req.body,image:req.session.img,showId:req.session.us_id})
        res.redirect('/product')
    }
    async addFriend(req,res){
        let x=await Friend.create({from_id:req.session.us_id,to_id:req.body.id})
        res.redirect('/search')
    }
    async acceptFr(req,res){
        await Friend.update({
            status:1
        },
        {
            where:{
                from_id:req.query.id,
                to_id:req.session.us_id,
            
        }})
        res.redirect('/request')
    }
    async cancelFr(req,res){
        await Friend.destroy({
            where:{
                from_id:req.query.id,
                to_id:req.session.us_id,
                }
            }
        )
        res.redirect('/request')
    }
    async deleteFr(req,res){
        await Friend.destroy({
            where:{
                from_id:req.query.id,
                to_id:req.session.us_id,
            }
        })
        res.redirect('/friends')
    }
    async logOut(req,res){
        delete req.session.us_id;
        res.redirect('/')
    }
}

module.exports= new PostController