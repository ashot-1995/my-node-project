const { User, Product, Friend } = require("../model/user");
const {Op}=require('sequelize');
const e = require("connect-flash");

class GetController{
    constructor(){
    }
    async getIndex(req,res){
        res.render('index',{obj:req.session.error})
    }
    async getRegister(req,res){
        let ob={};
        if(req.session.errors){
            for(let e of req.session.errors){
                if(ob[e.param]==undefined){
                    ob[e.param]=e.msg
                }
            }
        }
        res.render('register',{ob})
    }
    async getProfile(req,res){
        let x=await Product.findAll({})
        let user = await User.findOne({where:{id: req.session.us_id}})
        
        res.render('profile',{user:user.dataValues,arr:x})
    }
    async getProduct(req,res){
        let x=await Product.findAll({where:{showId:req.session.us_id}})
        res.render('product',{arr:x})
    }
    async getFriends(req,res){
        let fr=await Friend.findAll({
            where:{
                [Op.or]: [
                    { from_id: req.session.us_id}, 
                    { to_id: req.session.us_id}
                ],
                status:1
            }
        })
        let us=await User.findAll()
        let arr=[]
        for(let e of fr){
            let a=us.filter(elm=>elm.dataValues.id==e.dataValues.from_id && e.dataValues.from_id!=req.session.us_id)
            if(a.length>0){
                arr.push({...a[0].dataValues})
            }else{
                let a=us.filter(elm=>elm.dataValues.id==e.dataValues.to_id)
                arr.push({...a[0].dataValues})
            }
        }
        res.render('friends',{arr})
    }
    async getChat(req,res){
        let fr=await Friend.findAll({
            where:{
                [Op.or]: [
                    { from_id: req.session.us_id}, 
                    { to_id: req.session.us_id}
                ],
                status:1
            }
        })
        let us=await User.findAll()
        let arr=[]
        for(let e of fr){
            let a=us.filter(elm=>elm.dataValues.id==e.dataValues.from_id && e.dataValues.from_id!=req.session.us_id)
            if(a.length>0){
                arr.push({...a[0].dataValues})
            }else{
                let a=us.filter(elm=>elm.dataValues.id==e.dataValues.to_id)
                arr.push({...a[0].dataValues})
            }
        }
        console.log("*************************",arr, fr)
        res.render('chat',{arr,userId:req.session.us_id})
    }
    async getRequest(req,res){
        let fr=await Friend.findAll({
            where:{
                to_id:req.session.us_id, status:0
            }
        })
        let us=await User.findAll()
        let arr=[]
        for(let e of fr){
            let a=us.filter(elm=>elm.dataValues.id==e.dataValues.from_id)
            arr.push(a[0].dataValues)
        }
        res.render('request',{arr,userId:req.session.us_id})
    }
    async search(req,res){
        let x=await User.findAll(
            {
                where:{
                    name:{
                        [Op.startsWith]:req.query.name
                    }
                }
            }
        )
        let us = await Friend.findAll({
            where:{
                [Op.or]: [{ from_id: req.session.us_id }, { to_id: req.session.us_id }],    
            }
        })
        let arr =[]
        for(let obj of x){
            let a = us.filter(e=>(e.dataValues.from_id==obj.dataValues.id && e.dataValues.to_id== req.session.us_id) 
            || (e.dataValues.to_id==obj.dataValues.id && e.dataValues.from_id== req.session.us_id))
            if(a.length>0){
                arr.push({...obj.dataValues, ...a[0].dataValues, isFriends:true})
            }else{
                arr.push({...obj.dataValues, isFriends:false})
            }
        }
        res.render("search", {arr})
    }
    async adminProfile(req,res){
        res.render('adminProfile')
    }
}

module.exports=new GetController;