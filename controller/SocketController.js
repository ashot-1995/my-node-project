const { Op } = require("sequelize");
const { Message, Friend, User } = require("../model/user");


class SocketController{
    constructor(io){
        this.io=io
        io.on('connection',(soket)=>{
            this.socket=soket;
            this.socket.on('joined',(data)=>{
                this.joined(data)
            })
            this.socket.on('start',(data)=>{
                this.getMessage(data);
            })
            this.socket.on('newMess',(data)=>{
                this.newMess(data)
            })
            this.socket.on('startReq',(data)=>{
                this.getRequest(data)
            })
            this.socket.on('acceptReq',(data)=>{
                this.acceptReq(data)
            })
            this.socket.on('cancelReq',(data)=>{
                this.cancelReq(data)
            })
        })   
    }
    joined(data){
        this.socket.join(data.id)
    }
    async getMessage(data){
        await Message.findAll({where:{
            [Op.or]:[{from_id:data.id,to_id:data.fid},{from_id:data.fid,to_id:data.id}]
            }
        }).then(res=>{
            console.log(res)
            this.io.to(data.id).to(data.fid).emit('getMess',res)
            // this.socket.broadcast.emit('getMess',res)
        }).catch(e=>{
            console.log(e)
        })
    }
    async newMess(data){
        await Message.create({message:data.text,from_id:data.id,to_id:data.fid})
        .then(()=>{
            this.getMessage(data)
        })
        .catch((e)=>{
            console.log(e)
        })
    }
    async getRequest(data){

        // await Friend.findAll({where:{
        //     to_id:data.id,
        //     status:false,
        // }})
        await Friend.findAll({
            where:{
                to_id:data.id,
                status:false,
            }    
        })
        .then(async res=>{
            let arr=[]
            for(let e of res){
                let us=await User.findOne({
                    where:{
                        id:e.from_id
                    }
                })
                if(us){
                    arr.push(us)
                }
            }
            this.io.to(data.id).emit('getReq',arr)
        })
        .catch(e=>{
            console.log(e)
        })
    }
    async acceptReq(data){
        await Friend.update({
                status:1,
            },
        {
            where:{
                from_id:data.id
            }
        })
        .then(()=>{
            this.getRequest(data)
        })
        .catch((e)=>{
            console.log(e)
        })
    }
    async cancelReq(data){
        await Friend.destroy(
        {
            where:{
                from_id:data.id
            }
        })
        .then(()=>{
            this.getRequest(data)
        })
        .catch((e)=>{
            console.log(e)
        })
    }
}
module.exports=SocketController