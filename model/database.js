const mysql=require('mysql')

class Database{
    constructor(){
        this.db=mysql.createConnection({
            host:'localhost',
            port:3306,
            user:'root',
            password:'',
            database:'my_works'
        })
    }
   
     findAll(table){
        return new Promise((resolve,reject)=>{
            this.db.query(`select * from ${table}`, (err,data)=>{
                if(err) throw err
                resolve(data)
            })
        })
    }
    // findBy(s1,s2){
    //     return new Promise((resolve,reject)=>{
    //         this.db.query(`select * from user where email='${s1}' and password='${s2
            
    //         }'`, (err,data)=>{
    //             if(err) throw err
    //             resolve(data)
    //         })

    //     })
    // }
    findBy(table,data){
        let query=`select * from ${table} where `
        for(let e in data){
            query+=`${e}='${data[e]}' and `
        }
        query=query.substring(0,query.length-4)
        return new Promise((resolve,reject)=>{
            this.db.query(query,(err,data)=>{
                if(err) throw err;
                resolve(data)
            })
        })
    }
    insert(table,data){
        let query=`insert into ${table}(`
        let key=Object.keys(data)
        let val=Object.values(data)
        key=key.join(',')
        val=val.join("','")
        query+=key+`)values('${val}')`
        this.db.query(query);
    }

}
module.exports=new Database