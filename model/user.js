const {Sequelize,Model,DataTypes}=require('sequelize')
const sequelize=new Sequelize('my_works','root','',{
    host:'localhost',
    dialect: 'mysql'
})
class User extends Model{}
User.init({
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:{
            type:DataTypes.STRING
        },
        surname:DataTypes.STRING,
        age:DataTypes.INTEGER,
        email:DataTypes.STRING,
        type:{
            type:DataTypes.INTEGER,
            defaultValue:0,
        },
        password:DataTypes.STRING,
        image:DataTypes.STRING,
    },
    {
        modelName:'show',
        sequelize
    }
)
User.sync()
class Product extends Model{}
Product.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        name:DataTypes.STRING,
        price:DataTypes.DOUBLE,
        count:DataTypes.INTEGER,
        description:DataTypes.STRING,
        image:DataTypes.STRING,
    },
    {
        
        modelName:'production',
        sequelize

    }
)
Product.belongsTo(User)
Product.sync()

class Friend extends Model{}
Friend.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true,
        },
       status:{
           type:DataTypes.BOOLEAN,
           defaultValue:false
       } 
    },{
        modelName:'friend',
        sequelize
    }
)

Friend.belongsTo(User, {targetKey:"id", foreignKey:"from_id"})
Friend.belongsTo(User, {targetKey:"id", foreignKey:"to_id"})
Friend.sync();

class Message extends Model{}
Message.init(
    {
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true,
        },
        message:{
           type:DataTypes.STRING,
       } 
    },{
        modelName:'message',
        sequelize
    }
)

Message.belongsTo(User, {targetKey:"id", foreignKey:"from_id"})
Message.belongsTo(User, {targetKey:"id", foreignKey:"to_id"})
Message.sync();

module.exports={User,sequelize,Product,Friend,Message}
