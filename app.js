const express=require('express')
const router=require('./routers/router.js')
const app=express()
const port=8080;
const session=require('express-session')
const bodyParser=require('body-parser')
const flash=require('connect-flash')
const passport=require('passport')
const SocketController=require('./controller/SocketController.js')
const LocalStrategy=require('passport-local').Strategy
const server=require('http').Server(app)
const io=require('socket.io')(server,{
    cors:{
        method:['POST','GET'],
        port:'http://localhost:8080'
    }
})

new SocketController(io)

const handlebars=require('express-handlebars').create({defaultLayout:'main',extname:'.hbs', partialsDir:__dirname+"/views/parent/"});
app.engine('hbs',handlebars.engine)

app.set('view engine','hbs')
app.set('views','views')//arajin bary partadira
app.use(express.static('public'))//partadir toxa


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
app.use(session({secret:"+", resave:true, saveUninitialized:true}))

app.use(passport.initialize())
app.use(passport.session())

app.use(flash());
app.use((req, res, next)=>{
    res.locals.succ = req.flash("success_msg")
    res.locals.err = req.flash("error_msg")
    next()
})

app.use('/',router)



server.listen(port)